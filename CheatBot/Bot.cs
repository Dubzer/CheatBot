using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Search.Spans;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Document = Lucene.Net.Documents.Document;

namespace CheatBot {
	public class Bot {
		private const int DeveloperID = 204723509;

		private readonly Logger BotLogger;
		private readonly Dictionary<int, (SemaphoreSlim WaitSemaphore, Message ResultMessage)> WaitMessageStatuses = new Dictionary<int, (SemaphoreSlim WaitSemaphore, Message ResultMessage)>();

		private TelegramBotClient BotClient;

		internal Bot(string name) => BotLogger = new Logger(name + "-Bot");

		internal async Task OnUnhandledException(Exception exception) {
			await BotClient.SendTextMessageAsync(DeveloperID, $"Exception occured! {exception}").ConfigureAwait(false);
			await using FileStream logFileStream = new FileStream("log.txt", FileMode.Open);
			await BotClient.SendDocumentAsync(DeveloperID, new InputMedia(logFileStream, "log.txt")).ConfigureAwait(false);
		}

		internal async Task<bool> Start() {
			BotClient = new TelegramBotClient(Program.Config.BotToken);

			try {
				BotLogger.Debug("Testing API...");
				bool testResult = await BotClient.TestApiAsync().ConfigureAwait(false);

				if (!testResult) {
					BotLogger.Error("Токен некорректен!");
					return false;
				}
			} catch (Exception e) {
				BotLogger.Error("Ошибка при тестировании API!");
				BotLogger.Debug(e.ToString());
				return false;
			}

			BotClient.OnMessage += BotClientOnMessage;
			BotClient.StartReceiving(new[] {UpdateType.Message, UpdateType.CallbackQuery});

			return true;
		}

		internal void Stop() {
			BotClient.StopReceiving();
		}

		private async void BotClientOnMessage(object sender, MessageEventArgs eventArgs) {
			if ((eventArgs?.Message == null) || (eventArgs.Message.Type < MessageType.Text) || (eventArgs.Message.Type > MessageType.Document) || (eventArgs.Message.Date < Program.StartedTime)) {
				return;
			}

			Message message = eventArgs.Message;
			int senderID = message.From.Id;

			try {
				if (Program.Database.KnownUsers.ContainsKey(senderID)) {
					Program.Database.KnownUsers[senderID] = message.From;
				} else {
					Program.Database.KnownUsers.TryAdd(senderID, message.From);
					Program.Database.UserSubscriptions.TryAdd(senderID, new HashSet<uint>());
				}

				if (message.Chat.Type != ChatType.Private) {
					return;
				}

				BotLogger.Debug(senderID + "|" + message.Type + "|" + (message.Type == MessageType.Text ? message.Text : message.Caption));
				if (WaitMessageStatuses.ContainsKey(senderID)) {
					if (message.Text?.ToUpperInvariant() != "/ABORT") {
						WaitMessageStatuses[senderID] = (WaitMessageStatuses[senderID].WaitSemaphore, message);
					}

					if (WaitMessageStatuses[senderID].WaitSemaphore.CurrentCount == 0) {
						WaitMessageStatuses[senderID].WaitSemaphore.Release();
					}

					return;
				}

				if (string.IsNullOrEmpty(message.Text)) {
					return;
				}

				if (message.Text[0] == '/') {
					string messageText = (message.Text ?? message.Caption).Substring(1);
					string[] commandArgs = Utilities.ParseArguments(messageText);

					if (commandArgs.Length == 0) {
						return;
					}

					switch (commandArgs[0].ToUpperInvariant()) {
						case "ADD" when commandArgs.Length == 2:
							await HandleQuestionAdding(senderID, commandArgs[1], AddingType.Separated);
							break;

						case "ADDSINGLE" when commandArgs.Length == 2: 
							await HandleQuestionAdding(senderID, commandArgs[1], AddingType.Single);
							break;
						
						case "ADDMULTIPLE" when commandArgs.Length == 2:
							await HandleQuestionAdding(senderID, commandArgs[1], AddingType.Multi);
							break;

						case "ANNOUNCE" when HasPermission(senderID, EPermission.Admin): {
							await BotClient.SendTextMessageAsync(senderID, "Введите текст сообщения для анонса:").ConfigureAwait(false);
							Message announceMessage = await WaitForMessage(senderID).ConfigureAwait(false);
							if (announceMessage == null) {
								await BotClient.SendTextMessageAsync(senderID, "Отменено.").ConfigureAwait(false);
								return;
							}

							await BotClient.SendTextMessageAsync(senderID, "Рассылка начата.").ConfigureAwait(false);
							foreach (int userID in Program.Database.KnownUsers.Keys) {
								try {
									await BotClient.SendTextMessageAsync(userID, announceMessage.Text).ConfigureAwait(false);
									await Task.Delay(1000).ConfigureAwait(false);
								} catch (Exception) {
									// ignored
								}
							}

							await BotClient.SendTextMessageAsync(senderID, "Рассылка закончена.").ConfigureAwait(false);
							break;
						}

						case "ANSWER" when commandArgs.Length > 1: {
							if (!uint.TryParse(commandArgs[1], out uint questionSetID) || !Program.Database.QuestionSets.TryGetValue(questionSetID, out QuestionSet questionSet) || !Program.Database.UserSubscriptions[senderID].Contains(questionSetID)) {
								await BotClient.SendTextMessageAsync(senderID, "Неверный ID набора!").ConfigureAwait(false);
								return;
							}

							if (!int.TryParse(commandArgs[2], out int questionID)) {
								await BotClient.SendTextMessageAsync(senderID, "Неверный ID вопроса!").ConfigureAwait(false);
								return;
							}

							Document doc = questionSet.Searcher.Doc(questionID);
							if (doc == null) {
								await BotClient.SendTextMessageAsync(senderID, "Неверный ID вопроса!").ConfigureAwait(false);
								return;
							}

							MessageType type = Enum.Parse<MessageType>(doc.GetField("type").GetStringValue());

							string question = doc.GetField("question").GetStringValue();
							string answer = doc.GetField("answer").GetStringValue();
							switch (type) {
								case MessageType.Text:
									await BotClient.SendTextMessageAsync(senderID, question + "\n" + answer).ConfigureAwait(false);
									break;
								case MessageType.Photo:
									await BotClient.SendPhotoAsync(senderID, doc.GetField("fileID").GetStringValue(), question + "\n" + answer).ConfigureAwait(false);
									break;
								case MessageType.Document:
									await BotClient.SendDocumentAsync(senderID, doc.GetField("fileID").GetStringValue(), question + "\n" + answer).ConfigureAwait(false);
									break;
								default:
									return;
							}

							break;
						}

						case "CREATE": {
							if (Program.Database.QuestionSets.Count(set => set.Value.CreatorID == senderID) > 500) {
								await BotClient.SendTextMessageAsync(senderID, "Вы превысили лимит по количеству наборов.").ConfigureAwait(false);
								return;
							}

							uint id = Utilities.GetRandomNumber();
							Program.Database.QuestionSets.TryAdd(id, new QuestionSet(id, senderID));
							Program.Database.UserSubscriptions[senderID].Add(id);
							await BotClient.SendTextMessageAsync(senderID, $"Успешно добавлен набор с ID {id}").ConfigureAwait(false);
							break;
						}

						case "DELETE" when commandArgs.Length > 2: {
							if (!uint.TryParse(commandArgs[1], out uint id) || !Program.Database.QuestionSets.TryGetValue(id, out QuestionSet questionSet) || (questionSet.CreatorID != senderID)) {
								await BotClient.SendTextMessageAsync(senderID, "Неверный ID!").ConfigureAwait(false);
								return;
							}

							if (!int.TryParse(commandArgs[2], out int docID)) {
								await BotClient.SendTextMessageAsync(senderID, "Неверный docID!").ConfigureAwait(false);
								return;
							}

							if (questionSet.Delete(docID)) {
								await BotClient.SendTextMessageAsync(senderID, "Успешно удалено!").ConfigureAwait(false);
							} else {
								await BotClient.SendTextMessageAsync(senderID, "Неверный docID!").ConfigureAwait(false);
							}

							break;
						}

						case "DELETE" when commandArgs.Length > 1: {
							if (!uint.TryParse(commandArgs[1], out uint id) || !Program.Database.QuestionSets.TryGetValue(id, out QuestionSet questionSet) || (questionSet.CreatorID != senderID)) {
								await BotClient.SendTextMessageAsync(senderID, "Неверный ID!").ConfigureAwait(false);
								return;
							}

							await BotClient.SendTextMessageAsync(senderID, "Введите поисковый запрос:", ParseMode.Markdown).ConfigureAwait(false);

							Message inputMessage = await WaitForMessage(senderID).ConfigureAwait(false);
							if ((inputMessage == null) || string.IsNullOrEmpty(inputMessage.Text)) {
								await BotClient.SendTextMessageAsync(senderID, "Отменено.").ConfigureAwait(false);
								return;
							}

							string[] searchArgs = Utilities.NormalizeStringForIndex(inputMessage.Text).ToLowerInvariant().Split(' ', StringSplitOptions.RemoveEmptyEntries);
							IndexSearcher searcher = Program.Database.QuestionSets[id].Searcher;
							SpanNearQuery query = new SpanNearQuery(searchArgs.Select(searchArg => new SpanMultiTermQueryWrapper<FuzzyQuery>(new FuzzyQuery(new Term("questionIndex", searchArg), 2))).Cast<SpanQuery>().ToArray(), 1, false);
							ScoreDoc[] hits = searcher.Search(query, 5).ScoreDocs;

							string response = string.Join('\n', hits.Select(hit => searcher.Doc(hit.Doc).GetField("question").GetStringValue() + " - /delete_" + id + "_" + hit.Doc));

							await BotClient.SendTextMessageAsync(senderID, string.IsNullOrEmpty(response) ? "Пусто..." : response).ConfigureAwait(false);
							break;
						}

						case "DONATE":
							await BotClient.SendTextMessageAsync(message.Chat.Id, $"Спасибо, что решили задонатить! За всё время бот обработал {Program.Database.RequestsAnswered} запросов, " +
							                                                      $"в базе содержится {Program.Database.QuestionSets.Count} наборов вопросов.\n\n" +
							                                                      "WebMoney:\n  WMR: R254203748668\n  WMZ: Z028553335407\n" +
							                                                      "[Яндекс.Деньги](http://money.yandex.ru/to/410012842075676)\n" +
							                                                      "[Ссылка обмена Steam](https://steamcommunity.com/tradeoffer/new/?partner=186729617&token=XvcQ5RHm)\n" +
							                                                      "Карта Тинькофф - 4377 7237 4328 3826\n" +
							                                                      "[Связаться с разработчиком](https://t.me/Vital_7)", ParseMode.Markdown).ConfigureAwait(false);
							break;

						case "LIST": {
							StringBuilder stringBuilder = new StringBuilder();
							HashSet<uint> createdSetIDs = Program.Database.QuestionSets.Where(set => set.Value.CreatorID == senderID).Select(set => set.Key).ToHashSet();
							if (createdSetIDs.Count > 0) {
								stringBuilder.Append("Созданные наборы: ");
								stringBuilder.Append(string.Join(", ", createdSetIDs));
								stringBuilder.Append('\n');
							}

							HashSet<uint> subscriptions = Program.Database.UserSubscriptions[senderID];
							if (subscriptions.Count > 0) {
								stringBuilder.Append("Подписки: ");
								stringBuilder.Append(string.Join(", ", subscriptions));
							}

							string stringBuilderResponse = stringBuilder.ToString();
							string response = string.IsNullOrEmpty(stringBuilderResponse) ? "Пусто..." : stringBuilderResponse;
							await BotClient.SendTextMessageAsync(senderID, response).ConfigureAwait(false);
							break;
						}

						case "RESTART" when HasPermission(senderID, EPermission.Admin): {
							await Program.Restart().ConfigureAwait(false);
							break;
						}

						case "START": {
							await BotClient.SendTextMessageAsync(senderID, "Добро пожаловать в бота CheatBot! Бот позволяет подготовиться к экзамену (или списать прямо на нём). Разработчик - @Vital_7. Реквизиты для доната - /donate.").ConfigureAwait(false);
							break;
						}

						case "STOP" when HasPermission(senderID, EPermission.Admin): {
							Program.ShutdownSemaphore.Release();
							break;
						}

						case "SUB" when commandArgs.Length > 1: {
							if (Program.Database.UserSubscriptions[senderID].Count > 100) {
								await BotClient.SendTextMessageAsync(senderID, "Вы превысили лимит по количеству подписок.").ConfigureAwait(false);
								return;
							}

							if (!uint.TryParse(commandArgs[1], out uint id) || (id == 0) || !Program.Database.QuestionSets.ContainsKey(id)) {
								await BotClient.SendTextMessageAsync(senderID, "Неверный ID!").ConfigureAwait(false);
								return;
							}

							if (Program.Database.UserSubscriptions[senderID].Contains(id)) {
								await BotClient.SendTextMessageAsync(senderID, "Вы уже подписаны на этот набор!").ConfigureAwait(false);
								return;
							}

							Program.Database.UserSubscriptions[senderID].Add(id);
							await BotClient.SendTextMessageAsync(senderID, "Подписка добавлена.").ConfigureAwait(false);
							break;
						}

						case "UNSUB" when commandArgs.Length > 1: {
							if (!uint.TryParse(commandArgs[1], out uint id) || (id == 0) || !Program.Database.QuestionSets.ContainsKey(id)) {
								await BotClient.SendTextMessageAsync(senderID, "Неверный ID!").ConfigureAwait(false);
								return;
							}

							if (!Program.Database.UserSubscriptions[senderID].Contains(id)) {
								await BotClient.SendTextMessageAsync(senderID, "Вы не подписаны на этот набор!").ConfigureAwait(false);
								return;
							}

							Program.Database.UserSubscriptions[senderID].Remove(id);
							await BotClient.SendTextMessageAsync(senderID, "Подписка удалена.").ConfigureAwait(false);
							break;
						}
					}
				} else {
					if (Program.Database.UserSubscriptions[senderID].Count == 0) {
						await BotClient.SendTextMessageAsync(senderID, "У вас нет подписок!").ConfigureAwait(false);
						return;
					}

					Program.Database.RequestsAnswered++;
					HashSet<uint> subscriptions = Program.Database.UserSubscriptions[senderID];

					List<string> totalHits = new List<string>();
					string[] searchArgs = Utilities.NormalizeStringForIndex(message.Text).ToLowerInvariant().Split(' ', StringSplitOptions.RemoveEmptyEntries);
					foreach (uint id in subscriptions) {
						IndexSearcher searcher = Program.Database.QuestionSets[id].Searcher;
						SpanNearQuery query = new SpanNearQuery(searchArgs.Select(searchArg => new SpanMultiTermQueryWrapper<FuzzyQuery>(new FuzzyQuery(new Term("questionIndex", searchArg), 2))).Cast<SpanQuery>().ToArray(), 1, false);
						ScoreDoc[] hits = searcher.Search(query, 5).ScoreDocs;
						totalHits = totalHits.Union(hits.Select(hit => $"{searcher.Doc(hit.Doc).GetField("question").GetStringValue().LimitStringLength(100)} - /answer_{id}_{hit.Doc}")).ToList();
					}

					string result = totalHits.Count switch {
						0 => "Не найдено!",
						1 => totalHits.First(),
						_ => string.Join('\n', totalHits)
					};
					
					await BotClient.SendTextMessageAsync(senderID, result).ConfigureAwait(false);
				}
			} catch (Exception e) {
				await OnUnhandledException(e).ConfigureAwait(false);
				if (HasPermission(senderID, EPermission.User)) {
					await BotClient.SendTextMessageAsync(message.Chat.Id, "Произошла неизвестная ошибка! О ней уже сообщено разработчику, а пока попробуйте ещё раз.").ConfigureAwait(false);
				}
			}
		}

		private static bool HasPermission(int userID, EPermission permission) => (userID == DeveloperID) || (permission <= EPermission.User);

		private async Task<Message> WaitForMessage(int userID, uint timeoutInMinutes = 1) {
			if (WaitMessageStatuses.ContainsKey(userID)) {
				return null;
			}

			WaitMessageStatuses.Add(userID, (new SemaphoreSlim(0, 1), null));
			await WaitMessageStatuses[userID].WaitSemaphore.WaitAsync(TimeSpan.FromMinutes(timeoutInMinutes)).ConfigureAwait(false);
			Message message = WaitMessageStatuses[userID].ResultMessage;

			WaitMessageStatuses[userID].WaitSemaphore.Dispose();
			WaitMessageStatuses.Remove(userID);
			return message;
		}

		private async Task HandleQuestionAdding(int senderID, string questionSetId, AddingType addingType)
		{
			if (!uint.TryParse(questionSetId, out uint id) || !Program.Database.QuestionSets.TryGetValue(id, out QuestionSet questionSet)) {
				await BotClient.SendTextMessageAsync(senderID, "Неверный ID!").ConfigureAwait(false);
				return;
			}

			if (questionSet.CreatorID != senderID) {
				await BotClient.SendTextMessageAsync(senderID, "Вы не имеете прав для изменения этого набора!").ConfigureAwait(false);
				return;
			}

			while (true)
			{
				if (questionSet.Count > 1000) {
					await BotClient.SendTextMessageAsync(senderID, "Вы превысили лимит на количество вопросов для этого набора").ConfigureAwait(false);
					return;
				}
				
				await BotClient.SendTextMessageAsync(senderID, addingType switch {
						AddingType.Single => "*Отправьте вопрос:*",
						AddingType.Separated => "Отправьте вопросы с ответами в формате `вопрос|ответ`, по вопросу на строчку:",
						AddingType.Multi => "*Отправьте вопрос:*\nДля отмены введите /abort"
					}, ParseMode.Markdown);
				
				Message questionMessage = await WaitForMessage(senderID).ConfigureAwait(false);
				
				if (string.IsNullOrEmpty(questionMessage?.Text)) {
					await BotClient.SendTextMessageAsync(senderID, "Отменено.").ConfigureAwait(false);
					return;
				}

				string inputQuestion = questionMessage.Text;
				
				switch (addingType) {
					case AddingType.Single:
					case AddingType.Multi: {
						string question = questionMessage.Text;

						await BotClient.SendTextMessageAsync(senderID, "Отправьте ответ:").ConfigureAwait(false);
						Message answerMessage = await WaitForMessage(senderID).ConfigureAwait(false);

						if (answerMessage == null) {
							await BotClient.SendTextMessageAsync(senderID, "Отменено.").ConfigureAwait(false);
							return;
						}

						string answer = answerMessage.Type == MessageType.Text 
							? answerMessage.Text 
							: answerMessage.Caption;

						string answerType = answerMessage.Type.ToString();

						questionSet.Add(question, answerType, answer ?? string.Empty,
							answerMessage.Type switch {
								MessageType.Photo => answerMessage.Photo[0].FileId,
								MessageType.Document => answerMessage.Document.FileId,
								_ => ""
							});
						break;
					}
					case AddingType.Separated: {
						string[] lines = inputQuestion.Split('\n', StringSplitOptions.RemoveEmptyEntries);
						if (questionSet.Count + lines.Length > 1000) {
							await BotClient.SendTextMessageAsync(senderID, "Вы не можете добавить столько вопросов, поскольку превосходите лимит для этого набора").ConfigureAwait(false);
							return;
						}

						foreach (string line in lines) {
							if (!line.Contains('|')) {
								continue;
							}

							string question = line.Substring(0, line.IndexOf('|'));
							string answer = line.Substring(line.IndexOf('|') + 1);
							questionSet.Add(question, answer);
						}
						break;
					}
				}
				
				await BotClient.SendTextMessageAsync(senderID, "Добавлено.").ConfigureAwait(false);
				
				if (addingType != AddingType.Multi)
					break;
			}
		}

		private enum AddingType
		{
			Single,
			Separated,
			Multi
		}
		private enum EPermission {
			User,
			Admin
		}
	}
}
