using System;
using System.IO;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Analysis.Util;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Lucene.Net.Util;
using Newtonsoft.Json;
using Directory = System.IO.Directory;

namespace CheatBot {
	internal class QuestionSet : IDisposable {
		private static readonly StandardAnalyzer Analyzer = new StandardAnalyzer(LuceneVersion.LUCENE_48, CharArraySet.EMPTY_SET);

		private static readonly IndexWriterConfig WriterConfig = new IndexWriterConfig(LuceneVersion.LUCENE_48, Analyzer) {
			OpenMode = OpenMode.CREATE_OR_APPEND
		};

		private readonly FSDirectory IndexFolder;
		private readonly IndexWriter IndexWriter;

		[JsonConstructor]
		internal QuestionSet(uint id, int creatorID) {
			ID = id;
			CreatorID = creatorID;
			string indexPath = Path.Join(Directory.GetCurrentDirectory(), "indexes", ID.ToString());
			if (!Directory.Exists(indexPath)) {
				Directory.CreateDirectory(indexPath);
			}

			IndexFolder = FSDirectory.Open(indexPath);
			IndexWriter = new IndexWriter(IndexFolder, (IndexWriterConfig) WriterConfig.Clone());
		}

		[JsonProperty]
		internal uint ID { get; private set; }

		[JsonProperty]
		internal int CreatorID { get; private set; }

		[JsonProperty]
		private int IncrementalID { get; set; }

		internal int Count => IndexWriter.NumDocs;

		private DirectoryReader Reader => IndexWriter.GetReader(true);
		internal IndexSearcher Searcher => new IndexSearcher(Reader);

		public void Dispose() {
			IndexWriter.Commit();
			IndexFolder?.Dispose();
			IndexWriter?.Dispose();
		}

		internal void Add(string question, string answer, string type = "Text", string fileID = "") {
			Document document = new Document {
				new TextField("questionIndex", Utilities.NormalizeStringForIndex(question), Field.Store.NO),
				new StringField("question", question, Field.Store.YES),
				new StringField("answer", answer, Field.Store.YES),
				new StringField("type", type, Field.Store.YES),
				new StringField("fileID", fileID, Field.Store.YES)
			};

			IndexWriter.AddDocument(document);
		}

		internal bool Delete(int docID) => IndexWriter.TryDeleteDocument(Reader, docID);
	}
}
