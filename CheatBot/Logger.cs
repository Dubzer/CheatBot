using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace CheatBot {
	internal class Logger {
	#if DEBUG
		private static readonly bool Debugging = true;
	#else
		private static readonly bool Debugging = false;
	#endif

		internal Logger(string loggerName) => LoggerName = loggerName;

		private static readonly object LockFile = new object();
		private readonly string LoggerName;

		internal void Info(string text, [CallerMemberName] string method = null) {
			Log(string.IsNullOrEmpty(LoggerName) ? text : LoggerName + "|" + text, Level.Info, method);
		}

		internal void Error(string text, [CallerMemberName] string method = null) {
			Log(string.IsNullOrEmpty(LoggerName) ? text : LoggerName + "|" + text, Level.Error, method);
		}

		internal void Debug(string text, [CallerMemberName] string method = null) {
			Log(string.IsNullOrEmpty(LoggerName) ? text : LoggerName + "|" + text, Level.Debug, method);
		}

		private static void Log(string text, Level level, string method) {
			string stringToLog = $"[{DateTime.Now:G}]|{Enum.GetName(typeof(Level), level).ToUpperInvariant()}|{method}|{text}";
			Console.ForegroundColor = level switch {
				Level.Info => ConsoleColor.White,
				Level.Error => ConsoleColor.Red,
				Level.Debug => ConsoleColor.Gray,
				_ => Console.ForegroundColor
			};

			if ((level != Level.Debug) || Debugging) {
				Console.WriteLine(stringToLog);
			}

			lock (LockFile) {
				File.AppendAllText("log.txt", stringToLog + "\r\n");
			}
		}

		private enum Level {
			Info,
			Error,
			Debug
		}
	}
}
