using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using Telegram.Bot.Types;
using File = System.IO.File;

namespace CheatBot {
	public class Database : IDisposable {
		private readonly object LockSave = new object();
		private readonly Timer SaveTimer;

		[JsonProperty]
		internal uint RequestsAnswered;

		[JsonConstructor]
		internal Database() {
			SaveTimer = new Timer(e => Save(), null, TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(10));
		}

		[JsonProperty]
		internal ConcurrentDictionary<int, User> KnownUsers { get; private set; } = new ConcurrentDictionary<int, User>();

		[JsonProperty]
		internal ConcurrentDictionary<uint, QuestionSet> QuestionSets { get; private set; } = new ConcurrentDictionary<uint, QuestionSet>();

		[JsonProperty]
		internal ConcurrentDictionary<int, HashSet<uint>> UserSubscriptions { get; private set; } = new ConcurrentDictionary<int, HashSet<uint>>();

		public void Dispose() {
			Save();
			SaveTimer?.Dispose();
		}

		internal static Database Create() {
			Database database = new Database();
			return database;
		}

		internal static Database Load() {
			if (!File.Exists("data.json")) {
				return null;
			}

			string content = File.ReadAllText("data.json");
			Database loadedDatabase;
			try {
				loadedDatabase = JsonConvert.DeserializeObject<Database>(content);
			} catch {
				return null;
			}

			return loadedDatabase;
		}

		internal void Save() {
			lock (LockSave) {
				string content = JsonConvert.SerializeObject(this);
				File.WriteAllText("data.json", content);
			}
		}
	}
}
