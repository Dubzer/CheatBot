﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CheatBot {
	internal static class Program {
		internal static readonly SemaphoreSlim ShutdownSemaphore = new SemaphoreSlim(0, 1);
		internal static readonly DateTime StartedTime = DateTime.UtcNow;

		internal static Configuration Config { get; private set; }
		internal static Database Database { get; private set; }

		private static Bot Bot { get; set; }
		private static readonly Logger ProgramLogger = new Logger(nameof(Program));
		
		private static async void OnUnhandledException(object sender, UnhandledExceptionEventArgs e) {
			ProgramLogger.Error("Возникло исключение! " + (Exception) e.ExceptionObject);
			try {
				await Bot.OnUnhandledException((Exception) e.ExceptionObject).ConfigureAwait(false);
			} catch {
				// ignored
			}

			await Restart().ConfigureAwait(false);
		}

		internal static async Task Restart() {
			string processFileName = Process.GetCurrentProcess().MainModule?.FileName;
			string executableName = Path.GetFileNameWithoutExtension(processFileName);
			if (string.IsNullOrEmpty(executableName)) {
				return;
			}

			List<string> arguments = Environment.GetCommandLineArgs().Skip(executableName.Equals(nameof(CheatBot)) ? 1 : 0).ToList();
			if (!executableName.Equals(nameof(CheatBot))) {
				arguments[0] = "\"" + arguments[0] + "\"";
			}

			try {
				Process.Start(processFileName, string.Join(" ", arguments));
			} catch (Exception e) {
				ProgramLogger.Error(e.ToString());
			}

			ShutdownSemaphore.Release();
			await Task.Delay(2000).ConfigureAwait(false);
		}

		private static async Task Main() {
			ProgramLogger.Info("Starting...");
			CultureInfo.CurrentCulture = new CultureInfo("ru-RU");
			AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
			if (!Directory.Exists("indexes")) {
				Directory.CreateDirectory("indexes");
			}

			ProgramLogger.Debug("Loading config...");
			Configuration loadedConfig = Configuration.Load();
			if (loadedConfig == null) {
				ProgramLogger.Debug("Config not found, creating...");
				loadedConfig = Configuration.Create();
				loadedConfig.Save();
			}

			Config = loadedConfig;

			Database loadedDatabase = Database.Load();
			if (loadedDatabase == null) {
				ProgramLogger.Debug("Database not found, creating...");
				loadedDatabase = Database.Create();
				loadedDatabase.Save();
			}

			Database = loadedDatabase;

			Bot = new Bot(nameof(CheatBot));
			if (!await Bot.Start().ConfigureAwait(false)) {
				ProgramLogger.Error("Невозможно запустить бота!");
				return;
			}

			ProgramLogger.Info("Бот успешно запущен!");
			await ShutdownSemaphore.WaitAsync().ConfigureAwait(false);

			Bot.Stop();
			Database.Dispose();
			ShutdownSemaphore.Release();
			ShutdownSemaphore.Dispose();
			foreach (QuestionSet questionSet in Database.QuestionSets.Values) {
				try {
					questionSet.Dispose();
				} catch (Exception) {
					// ignored
				}
			}
		}
	}
}
