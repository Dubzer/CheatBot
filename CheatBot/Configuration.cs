using System;
using System.IO;
using Newtonsoft.Json;

namespace CheatBot {
	internal class Configuration {
		[JsonConstructor]
		private Configuration() {
		}

		[JsonProperty]
		internal string BotToken { get; private set; }

		internal static Configuration Create() {
			Configuration config = new Configuration();

			Console.WriteLine(nameof(BotToken) + ":");
			config.BotToken = Console.ReadLine();

			return config;
		}

		internal static Configuration Load() {
			if (!File.Exists("config.json")) {
				return null;
			}

			string content = File.ReadAllText("config.json");
			Configuration loadedConfig;
			try {
				loadedConfig = JsonConvert.DeserializeObject<Configuration>(content);
			} catch {
				return null;
			}

			if (string.IsNullOrEmpty(loadedConfig.BotToken)) {
				if (string.IsNullOrEmpty(loadedConfig.BotToken)) {
					Console.WriteLine(nameof(BotToken) + ":");
					loadedConfig.BotToken = Console.ReadLine();
				}

				loadedConfig.Save();
			}

			return loadedConfig;
		}

		internal void Save() {
			string content = JsonConvert.SerializeObject(this, Formatting.Indented);
			File.WriteAllText("config.json", content);
		}
	}
}
