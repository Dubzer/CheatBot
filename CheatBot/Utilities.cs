using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheatBot {
	public static class Utilities {
		private static readonly object LockRandom = new object();
		private static readonly Random Random = new Random();

		internal static string LimitStringLength(this string input, int length) => input.Length > length ? input.Remove(length - 1) + "…" : input;

		internal static uint GetRandomNumber() {
			uint number = 0;
			while ((number == 0) || Program.Database.QuestionSets.ContainsKey(number)) {
				byte[] buffer = new byte[4];
				lock (LockRandom) {
					Random.NextBytes(buffer);
				}

				number = BitConverter.ToUInt32(buffer);
			}

			return number;
		}

		internal static string[] ParseArguments(string input) {
			List<string> args = new List<string>();
			bool isEscaped = false;
			StringBuilder temporary = new StringBuilder();
			string[] splitInput = input.Split(new[] {' ', '_'}, StringSplitOptions.RemoveEmptyEntries);
			foreach (string word in splitInput) {
				if (!isEscaped) {
					if (word[0] != '"') {
						args.Add(word);
					} else {
						string tempWord = word.Substring(1);
						if (tempWord[^1] == '"') {
							args.Add(tempWord.Remove(tempWord.Length - 1));
						} else {
							isEscaped = true;
							temporary.Append(tempWord + ' ');
						}
					}
				} else {
					if (word[^1] != '"') {
						temporary.Append(word + ' ');
					} else {
						isEscaped = false;
						temporary.Append(word.Substring(0, word.Length - 1));
						args.Add(temporary.ToString());
						temporary = new StringBuilder();
					}
				}
			}

			if (temporary.Length > 0) {
				string temp = temporary.ToString().TrimEnd();
				args.Add(temp);
			}

			return args.ToArray();
		}

		internal static string NormalizeStringForIndex(string input) => string.Join(' ', new string(input.Select(titleChar => char.IsLetter(titleChar) ? titleChar : ' ').ToArray()).Split(' ', StringSplitOptions.RemoveEmptyEntries));
	}
}
